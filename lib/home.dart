import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'login.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomePage extends StatefulWidget {
  final userdata;
  HomePage({this.userdata});
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {

  final FirebaseAuth auth = FirebaseAuth.instance;

  final TextEditingController _nameCtrl = TextEditingController();
  final TextEditingController _amtCtrl = TextEditingController();
  final TextEditingController _infoNameCtrl = TextEditingController();
  final TextEditingController _infoAmtCtrl = TextEditingController();

  void _addDebt(context, debtfxn) {
    var dialog = SimpleDialog(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                child:  Row(
                  children: <Widget>[
                    Text('Add New Debt', style: TextStyle(fontSize: 18.0)),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              TextField(
                controller: _nameCtrl,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.face),
                  hintText: 'Name',
                ),
              ),
              TextField(
                controller: _amtCtrl,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: 'Amount',
                  prefixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 5.0, 0, 0),
                    child: Text('₹', style: TextStyle(fontSize: 30.0, color: Colors.grey)),
                  )
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                    child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                  child: MaterialButton(
                    splashColor: Colors.purple,
                    child: Text('ADD', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
                    onPressed: debtfxn
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
    
    showDialog(context: context, barrierDismissible: false, builder: (context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    
    final CollectionReference mainCol = Firestore.instance.collection('${widget.userdata.uid}');

    void _addDebtData() {
      if(_nameCtrl.text != '' || _amtCtrl.text != '') {
        var newDocID = mainCol.document().documentID;
        mainCol.document(newDocID).setData(
          {'name': _nameCtrl.text, 'total': _amtCtrl.text}
        );
        mainCol.document(newDocID).collection('for').document().setData(
          {'name': 'Initial', 'amount': _amtCtrl.text}
        );
        Navigator.pop(context);
        setState(() {    
          _nameCtrl.clear();   
          _amtCtrl.clear();
        });
      } else {
        Navigator.pop(context);
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                Icon(Icons.error),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text('All the fields are required!'),
                )
              ],
            ),
          )
        );
      }
    }

    return Material(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => this._addDebt(context, _addDebtData),
        ),
        appBar: AppBar(
          title: Text('Debt Tracker'),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: PopupMenuButton(
                child: ClipOval(
                  child: Image.network(widget.userdata.photoUrl),
                ),
                itemBuilder: (BuildContext context) {
                  return List.generate(1 , (i) => PopupMenuItem(
                    child: InkWell(
                      onTap: () {
                         auth.signOut().whenComplete(() {
                           Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                         });
                      },
                      child: Text("Sign Out"),
                    ),
                  ));
                },
              )
            )
          ],
        ),
        body: Container(
          color: Color(0xFFF2F2F2),
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: StreamBuilder(
              stream: mainCol.snapshots(),
              builder: (context, snapshot) {
                if(!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  if(snapshot.hasError) {
                    return Center(
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.error),
                          Text('Error Loading Data', style: TextStyle(color: Color(0xFF333333)))
                        ],
                      ),
                    );
                  }
                  if('${snapshot.data.documents}' == '[]') {
                    return Center(
                      child: Text('No Debts', style: TextStyle(color: Color(0xFF888888), fontSize: 35.0))
                    );
                  }
                  return ListView.builder(
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, i) {
                      return ExpansionTile(
                        title: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(snapshot.data.documents[i]['name'], style: TextStyle(fontWeight: FontWeight.bold)),
                            Expanded(child: Align(alignment: Alignment.centerRight,child: Text('₹ '+snapshot.data.documents[i]['total'], style: TextStyle(fontWeight: FontWeight.bold))))
                          ],
                        ),
                        children: <Widget>[
                          StreamBuilder(
                            stream: mainCol.document(snapshot.data.documents[i].documentID).collection('for').snapshots(),
                            builder: (context, snaps) {
                              if(!snaps.hasData) {
                                return Align(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              } else {
                                return ListTile(
                                  title: Text(snaps.data.documents[0]['name']),
                                  trailing: Text('₹${snaps.data.documents[0]['amount']}'),
                                );
                              }
                            },
                          ),
                          ListTile(
                            title: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Tooltip(
                                    message: 'Add new sub-debt',
                                      child: IconButton(
                                      icon: Icon(Icons.add),
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (context) {
                                            return SimpleDialog(
                                              contentPadding: EdgeInsets.all(10.0),
                                              children: <Widget>[
                                                Align(alignment: Alignment.center,child: Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Text('Add Debts Info',
                                                  style: TextStyle(
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                )),
                                                TextField(
                                                  controller: _infoNameCtrl,
                                                  decoration: InputDecoration(
                                                    prefixIcon: Icon(Icons.movie),
                                                    hintText: 'For',
                                                  ),
                                                ),
                                                TextField(
                                                  controller: _infoAmtCtrl,
                                                  keyboardType: TextInputType.number,
                                                  decoration: InputDecoration(
                                                    hintText: 'Amount',
                                                    prefixIcon: Padding(
                                                      padding: EdgeInsets.fromLTRB(15.0, 5.0, 0, 0),
                                                      child: Text('₹', style: TextStyle(fontSize: 27.0, color: Colors.grey)),
                                                    )
                                                  ),
                                                ),
                                                Align(
                                                  alignment: Alignment.centerRight,
                                                      child: Padding(
                                                      padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                                                      child: MaterialButton(
                                                      splashColor: Colors.purple,
                                                      child: Text('ADD', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
                                                      onPressed: () {
                                                        if(_infoNameCtrl.text != '' || _infoAmtCtrl.text != '') {
                                                          mainCol.document(snapshot.data.documents[i].documentID).collection('for').document().setData(
                                                            {'name': _infoNameCtrl.text, 'amount': _infoAmtCtrl.text}
                                                          );
                                                          mainCol.document(snapshot.data.documents[i].documentID).updateData({
                                                            'total': '${int.parse(snapshot.data.documents[i]['total'])+int.parse(_infoAmtCtrl.text)}'
                                                          });
                                                          Navigator.pop(context);
                                                          setState(() {    
                                                            _infoNameCtrl.clear();   
                                                            _infoAmtCtrl.clear();
                                                          });
                                                        } else {
                                                           Navigator.pop(context);
                                                            showDialog(
                                                              context: context,
                                                              builder: (context) => AlertDialog(
                                                                content: Row(
                                                                  children: <Widget>[
                                                                    Icon(Icons.error),
                                                                    Padding(
                                                                      padding: const EdgeInsets.only(left: 5.0),
                                                                      child: Text('All the fields are required!'),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            );
                                                        }
                                                      },
                                                    ),
                                                  ),
                                                )
                                              ],
                                            );
                                          }
                                        );
                                      },
                                    ),
                                  )
                                ),
                                Expanded(
                                  child: Tooltip(
                                    message: 'Show all sub-debts',
                                      child: IconButton(
                                      icon: Icon(Icons.more),
                                      splashColor: Colors.purple,
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (context) {
                                            return SimpleDialog(
                                              contentPadding: EdgeInsets.all(10.0),
                                              children: <Widget>[
                                                Align(alignment: Alignment.center,child: Text('Debts Info',
                                                style: TextStyle(
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold
                                                ),
                                                )),
                                                Container(
                                                  padding: EdgeInsets.only(top: 5.0),
                                                  height: 250.0,
                                                  width: 300.0,
                                                  child: StreamBuilder(
                                                    stream: mainCol.document(snapshot.data.documents[i].documentID).collection('for').snapshots(),
                                                    builder: (context, snap) {
                                                      return ListView.builder(
                                                        itemCount: snap.data.documents.length,
                                                        itemBuilder: (context, index) {
                                                          return ListTile(
                                                            title: Row(children: <Widget>[
                                                              Text(snap.data.documents[index]['name']),
                                                              Expanded(
                                                                child: Align(
                                                                  alignment: Alignment.centerRight,
                                                                  child: Text('₹${snap.data.documents[index]['amount']}'),
                                                                )
                                                              )
                                                            ],),
                                                            trailing: IconButton(
                                                              icon: Icon(Icons.delete),
                                                              onPressed: () {
                                                                setState(() {
                                                                  mainCol.document(snapshot.data.documents[i].documentID).updateData({
                                                                    'total': '${int.parse(snapshot.data.documents[i]['total'])-int.parse(snap.data.documents[index]['amount'])}'
                                                                  });
                                                                  if(index != 0) {
                                                                    mainCol.document(snapshot.data.documents[i].documentID).collection('for').document(snap.data.documents[index].documentID).delete();
                                                                  } else {
                                                                    mainCol.document(snapshot.data.documents[i].documentID).collection('for').document(snap.data.documents[index].documentID).updateData({
                                                                      'amount': '0'
                                                                    });
                                                                  }                      
                                                                });
                                                              },
                                                            )
                                                          );
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ), 
                                              ],
                                            );
                                          }
                                        );
                                      },
                                    ),
                                  )
                                ),
                                Expanded(
                                  child: Tooltip(
                                    message: 'Delete debt',
                                      child: IconButton(
                                      icon: Icon(Icons.delete),
                                      onPressed: () {
                                        showDialog(context: context, builder: (context) {
                                          return AlertDialog(
                                            content: Text('Are you sure to delete this debt?'),
                                            actions: <Widget>[
                                              MaterialButton(
                                                child: Text('CANCEL', style: TextStyle(fontWeight: FontWeight.bold)),
                                                onPressed: () => Navigator.pop(context),
                                              ),
                                              MaterialButton(
                                                child: Text('DELETE', style: TextStyle(fontWeight: FontWeight.bold)),
                                                onPressed: () { 
                                                  mainCol.document(snapshot.data.documents[i].documentID).delete();
                                                  Navigator.pop(context);
                                                },
                                              )
                                            ],
                                          );
                                        });
                                      },
                                    ),
                                  )
                                ),
                              ],
                            )
                          )
                        ],
                        backgroundColor: Colors.white
                      );
                    },
                  );
                }
              },
            )
          ),
        ),
      )
    );
  }
}