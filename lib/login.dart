import 'package:flutter/material.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'home.dart';

class LoginPage extends StatefulWidget {
@override
LoginPageState createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {

  final FirebaseAuth auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<FirebaseUser> _signIn() async {
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;

    FirebaseUser user = await auth.signInWithGoogle(
      idToken: gSA.idToken,
      accessToken: gSA.accessToken
    );
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Color(0xFFF2F2F2),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 30.0, right: 20.0, left: 20.0, bottom: 18.0),
              child: Image.asset('assets/logo.png'),
            ),
            Text('KEEP TRACK OF YOUR DEBTS', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Text('Debt Tracker helps you in keeping record of your money transactions, so that you don’t have to.', textAlign: TextAlign.center, style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w300)),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20.0, 60.0, 20.0, 30.0),
              child: MaterialButton(
                color: Colors.white,
                padding: EdgeInsets.all(10.0),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                splashColor: Colors.grey,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/google_logo.png', width: 25.0,),
                    Padding(
                      padding: EdgeInsets.only(left: 5.0),
                      child: Text('Sign in with Google', style: TextStyle(fontSize: 20.0),),
                    )
                  ],
                ),
                onPressed: () => _signIn().then((FirebaseUser user) {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => HomePage(userdata: user))
                  );
                }).catchError((e) => showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      contentPadding: EdgeInsets.all(5.0),
                      content: Text('Error: $e'),
                    )
                  )
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text('Debt Tracker v1.0')
                 // child: Text('Developed by Harwinder Singh'),
                ),
              ),
            )
          ],
        )
      ),
    );
  }
}
